#include<netinet/in.h>
#include<sys/socket.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<netinet/if_ether.h>
#include <time.h>

#define REDIRECT_IP "192.168.1.10"
#define REDIRECT_PORT 46666
unsigned char broadcast_mac[6] = {255, 255, 255, 255, 255, 255};
unsigned char router_mac[6] = {0x10, 0x13, 0x31, 0x55, 0x63, 0xd4};
 
void ProcessPacket(unsigned char* , int);
 



int sock_send_raw;
struct sockaddr_in serverAddress;


int main()
{
    int saddr_size , data_size;
    struct sockaddr saddr;
    unsigned char *buffer = (unsigned char *) malloc(65536);
    printf("Starting...\n");
	

	
	/*SNIFFER SOCKET*/
    int sock_raw = socket( AF_PACKET , SOCK_RAW , htons(ETH_P_ALL)) ;
    if(sock_raw < 0)
    {
        printf("Socket Error\n");
        return 1;
    }
	printf("Sniffer socket initialized\n");
	

	/*REDIRECT SOCKET*/
	sock_send_raw = socket(AF_INET, SOCK_DGRAM, 0);
	//int broadcastEnable=1;
	//int ret=setsockopt(sock_send_raw, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(REDIRECT_PORT);
	inet_aton(REDIRECT_IP, &serverAddress.sin_addr.s_addr);
	if(sock_send_raw < 0)
	{
		 perror("Socket UDP Error");
         return 1;
	}
	printf("Redirect socket initialized\n");

	
	int process = 1;
	printf("Sniffer started...\n");
    while(1)
    {
        saddr_size = sizeof saddr;
        //Receive a packet
        data_size = recvfrom(sock_raw , buffer , 65536 , 0 , &saddr , (socklen_t*)&saddr_size);

        if(data_size <0 )
        {
            printf("Recvfrom error , failed to get packets\n");
            return 1;
        }
		//Scarto la metà dei pacchetti (essendo un bridge lo sniffer duplica i pacchetti)
		if(process == 0)
		{
			process = 1;
			continue;
		}
		process = 0;
        ProcessPacket(buffer , data_size);
    }
    close(sock_raw);
    printf("Finished");
    return 0;
}
 
int CompareMac(unsigned char* mac1,unsigned char* mac2)
{
	return mac1[0] == mac2[0] && mac1[1] == mac2[1] && mac1[2] == mac2[2] && mac1[3] == mac2[3] && mac1[4] == mac2[4] && mac1[5] == mac2[5];
}

unsigned char source_mac[6], destination_mac[6];
void ProcessPacket(unsigned char* buffer, int size)
{
	destination_mac[0] 	= buffer[0];
	destination_mac[1] 	= buffer[1];
	destination_mac[2] 	= buffer[2];
	destination_mac[3] 	= buffer[3];
	destination_mac[4] 	= buffer[4];
	destination_mac[5] 	= buffer[5];
	source_mac[0] 		= buffer[6];
	source_mac[1] 		= buffer[7];
	source_mac[2] 		= buffer[8];
	source_mac[3] 		= buffer[9];
	source_mac[4] 		= buffer[10];
	source_mac[5] 		= buffer[11];

	//Il pacchetto non interessa il router quindi non è di internet
	if (CompareMac(destination_mac, router_mac) == 0 && CompareMac(source_mac, router_mac) == 0)
	{
		return;
	}
	//Il pacchetto è di tipo broadcast quindi non è di internet
	if (CompareMac(destination_mac, broadcast_mac) == 1 || CompareMac(source_mac, broadcast_mac) == 1)
	{
		return;
	}
	//REDIRECT
	if(sendto(sock_send_raw, buffer, size, 0, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0)
	{
		printf("sendto error , failed to send packets\n");
		return;
	}
}












